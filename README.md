# docker-openjdk-pnpm

A Docker image with OpenJDK, Node.JS, and pnpm on Debian.

## Repository

https://gitlab.com/shivjm/docker-openjdk-pnpm/

## Issues

https://gitlab.com/shivjm/docker-openjdk-pnpm/issues/

## Tags

See all tags at [Docker Hub
(shivjm/openjdk-pnpm)](https://hub.docker.com/repository/docker/shivjm/openjdk-pnpm).

## Example Dockerfiles

Simple:

```Dockerfile
FROM shivjm/node-openjdk-pnpm

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN pnpm install -r --frozen-lockfile --reporter silent

COPY . .

ENTRYPOINT ["pnpm", "start"]
```
