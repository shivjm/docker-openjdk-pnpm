ARG OPENJDK_VERSION

FROM openjdk:${OPENJDK_VERSION}

ARG NODE_MAJOR_VERSION

LABEL org.opencontainers.image.authors="Shiv Jha-Mathur" \
  org.opencontainers.image.source="https://gitlab.com/shivjm/docker-java-pnpm/" \
  org.opencontainers.image.title="node-java-pnpm" \
  org.opencontainers.image.description="A minimal Docker image with Java, Node.JS, and pnpm on Alpine Linux"

RUN apt-get -qqy update && apt-get -qqy upgrade && apt-get install -qqy curl \
  && curl -sL https://deb.nodesource.com/setup_${NODE_MAJOR_VERSION}.x | bash - \
  && apt-get install -y nodejs \
  && npm i -g pnpm
